from bs4 import BeautifulSoup
from htmlmin import minify
from ics import Calendar
from ics.grammar.parse import ContentLine
import requests
import os

url = "https://indico.math.cnrs.fr/export/categ/390.ics?from=-62d"
c = Calendar(requests.get(url, verify=False).text)

for event in c.events:
    # Grab the html event page
    soup = BeautifulSoup(requests.get(event.url, verify=False).text, 'html.parser')
    # Search for the div that contains the description and use the html there
    # as the description for our event
    event.description = minify(str(soup.find(itemprop="description")))

    # Add a contact field to reference the original event.
    # Note that this is a fudge, I'm manipulating the contact field just so I
    # can get the main event URL to appear prominently. The CONTACT syntax is
    # CONTACT: <Name string>\; <Telephone>\; <email address>\; <website>
    # and you can only have one field of each type
    event.extra.append(
        ContentLine(name="CONTACT", value="EoCoE\; {}".format(event.url))
        )

    # Manipulate the ticketing feature of Time.ly to give additional prominent
    # links to the original page url
    event.extra.append(ContentLine(name="X-COST-TYPE", value="external"))
    event.extra.append(
        ContentLine(name="X-TICKETS-URL", value="{}".format(event.url))
        )

with open(os.path.join('EoCoE.ics'), 'w') as f:
    f.write(str(c))
